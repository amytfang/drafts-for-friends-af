<?php
/*
Plugin Name: Drafts for Friends
Plugin URI: http://automattic.com/
Description: Now you don't need to add friends as users to the blog in order to let them preview your drafts
Author: Neville Longbottom
Version: 2.2
Author URI: http://harrypotter.wikia.com/wiki/Neville_Longbottom
*/ 


class DraftsForFriends	{

	/**
	 * Constructor method that sets the initial hook that runs the `init` method.
	 * 
	 * @access public
	 */
	public function __construct(){
    	add_action( 'init', array( $this, 'init' ) );
	}

	/**
	 * Callback after Wordpress is mostly initiated that initiates the plugin, its hooks, and the admin and user options
	 * 
	 * @access public
	 * @return void
	 */
	public function init() {
		global $current_user;
		add_action( 'admin_menu',    array( $this, 'add_admin_pages'         ) );
		add_filter( 'the_posts',     array( $this, 'the_posts_intercept'     ) );
		add_filter( 'posts_results', array( $this, 'posts_results_intercept' ) );

		$this->admin_options = $this->get_admin_options();

		if ( $current_user->ID > 0 && isset( $this->admin_options[ $current_user->ID ] ) ) {
			$this->user_options = $this->admin_options[ $current_user->ID ];
		} else {
			$this->user_options = array();
		}

		$this->save_admin_options();
		$this->admin_page_init();
	}

	/**
	 * Sets up admin page, including the css and javacript and jQuery to be used by the plugin
	 * 
	 * @access public
	 * @return void
	 */
	public function admin_page_init() {
		wp_enqueue_script( 'jquery' );
		add_action( 'admin_head', array( $this, 'print_admin_css' ) );
		add_action( 'admin_head', array( $this, 'print_admin_js'  ) );
	}

	/**
	 * Gets shared posts saved in options, if any
	 * 
	 * @access public
	 * @return array Array of shared drafts with post_id, expiration time, and key
	 */
	public function get_admin_options() {
		$saved_options = get_option( 'shared' );
		return is_array( $saved_options ) ? $saved_options : array();
	}

	/**
	 * Saves shared posts in options
	 * 
	 * @access public
	 * @return void
	 */
    public function save_admin_options(){
        global $current_user;
        if ( $current_user->ID > 0 ) {
            $this->admin_options[ $current_user->ID ] = $this->user_options;
        }
        update_option( 'shared', $this->admin_options );
    }

	/**
	 * Add plugin admin page to sidebar menu
	 * 
	 * @access public
	 * @return void
	 */
	public function add_admin_pages(){
		add_submenu_page( "edit.php", __('Drafts for Friends', 'draftsforfriends'), __('Drafts for Friends', 'draftsforfriends'),
			'edit_posts', __FILE__, array($this, 'output_existing_menu_sub_admin_page') );
	}

	/**
     * Calculate seconds with specified the time unit and duration
     *
     * @access private
     * @param array {
	 * 		int 'expires' Duration
	 * 		string 'measure' Time unit
	 * }
     * @return int Number of seconds before it expires
     */
	private function get_seconds_delta( $params ) {
		$unit_count = 60;
		$multiplier = 60;

		if ( isset( $params['expires'] ) ) {
			$unit_count = intval( $params['expires'] );
		}

		$unit_multipliers = array( 
			's' => 1, 
			'm' => 60, 
			'h' => 3600, 
			'd' => 24 * 3600,
		);  

		if ( $params['measure'] && $unit_multipliers[ $params['measure'] ] ) {
			$multiplier = $unit_multipliers[ $params['measure'] ];
		}
		return $unit_count * $multiplier;
	}

	/**
     * Determines expiration status based on expiration time and current time
     *
     * @access private
     * @param int $end_time TimeStamp value
     * @return string expiration status
     */
	private function get_expiration_status( $end_time ) {
		$now_time = current_time('timestamp');

		if ( $now_time > $end_time ) {
			return __( 'Expired' ); 
		} else {
			return human_time_diff( $now_time, $end_time );
		}
	}

	/**
     * Processes an action to share a post and saves to options
     *
     * @access private
     * @param array $params Request parameters
     * @return void
     */
	private function process_post_options( $params ) {
		if ( ! wp_verify_nonce( $_POST['add_nonce'], 'drafts_for_friends_add' ) ) {
			return null; 
		}

		global $current_user;

		if ( $params['post_id'] ) {
			$post = get_post( $params['post_id'] );
			if ( ! $post ) {
				return __( 'There is no such post!', 'draftsforfriends' );
			}
			if ( 'publish' == get_post_status($post) ) {
				return __( 'The post is published!', 'draftsforfriends' );
			}
			$expire_time = time() + $this->get_seconds_delta( $params );
			$this->user_options['shared'][] = array(
				'id'      => $post->ID,
				'expires' => $expire_time,
				'key'     => 'baba_' . wp_generate_password( 8, false ),
			);
			$this->save_admin_options();
		}	
	}

	/**
     * Processes an action to delete a post and saves to options
     *
     * @access private
     * @param array $params Request parameters
     * @return void
     */
	private function process_delete( $params ) {
		if ( ! wp_verify_nonce( $_GET['nonce'], 'drafts_for_friends_delete' ) ) {
			return null; 
		}

		$shared = array();
		foreach( $this->user_options['shared'] as $share ) {
			if ( $share['key'] == $params['key'] ) {
				continue;
			}
			$shared[] = $share;	     
		}
		$this->user_options['shared'] = $shared;
		$this->save_admin_options();
	}

	/**
     * Processes an action to extend a post and saves to options
     *
     * @access private
     * @param array $params Request parameters
     * @return void
     */
	private function process_extend( $params ) {
		if ( ! wp_verify_nonce( $_POST['extend_nonce'], 'drafts_for_friends_extend' ) ) {
			return null; 
		}

		$shared = array();
		foreach( $this->user_options['shared'] as $share ) {
			if ( $share['key'] == $params['key'] ) {
				$share['expires'] += $this->get_seconds_delta( $params );
			}
			$shared[] = $share;
		}
		$this->user_options['shared'] = $shared;
		$this->save_admin_options();
	}

	/**
     * Gets all unpublished drafts for the current user
     *
     * @access private
     * @return array Array with 3 elements, drafts, scheduled, and pending post arrays
     */
	private function get_drafts() {
		global $current_user;

		$drafts = get_users_drafts( $current_user->ID );
		$scheduled = $this->get_users_future( $current_user->ID );
		$pending_params = array( 
			'post_author' => $current_user->ID, 
			'post_type'   => 'pending', 
		);
		$pending = get_posts( $pending_params );

		$all_unpublished = array(
			array(
				__('Your Drafts:', 'draftsforfriends'),
				count( $drafts ),
				$drafts,
			),
			array(
				__('Your Scheduled Posts:', 'draftsforfriends'),
				count( $scheduled ),
				$scheduled,
			),
			array(
				__('Pending Review:', 'draftsforfriends'),
				count( $pending ),
				$pending,
			),
		);
		return $all_unpublished; 
	}

	/**
     * Database query for all of the user's scheduled posts
     *
     * @access private
	 * @param int $user_id User ID
     * @return array Array of scheduled posts
     */
	private function get_users_future( $user_id ) {
		global $wpdb;
		return $wpdb->get_results("SELECT ID, post_title FROM $wpdb->posts WHERE post_type = 'post' AND post_status = 'future' AND post_author = $user_id ORDER BY post_modified DESC");
	}

	/**
	 * Get the current user's shared posts.
	 * 
	 * @access private
	 * @return array Array of shared posts
	 */
	private function get_shared() {
		return $this->user_options['shared'];
	}

	/**
	 * Display plugin admin page
	 * 
	 * @access public
	 * @return void
	 */
	public function output_existing_menu_sub_admin_page() {
		$message = '';
		if ( isset( $_POST['draftsforfriends_submit'] ) ) {
			$message = $this->process_post_options( $_POST );
		} elseif ( isset( $_POST['action'] ) && $_POST['action'] == 'extend' ) {
			$message = $this->process_extend( $_POST );
		} elseif ( isset( $_GET['action'] ) && $_GET['action'] == 'delete' ) {
			$message = $this->process_delete( $_GET );
		}
		$drafts = $this->get_drafts();
		
		?>
			<div class="wrap">
				<h2><?php esc_html_e( 'Drafts for Friends', 'draftsforfriends' ); ?></h2>
				<?php if ( $message ):?>
					<div id="message" class="updated fade"><?php echo esc_html( $message ); ?></div>
				<?php endif; ?>
				<h3><?php esc_html_e( 'Currently shared drafts', 'draftsforfriends' ); ?></h3>
				<table class="widefat">
					<thead>
						<tr>
							<th><?php esc_html_e( 'ID', 'draftsforfriends' ); ?></th>
							<th><?php esc_html_e( 'Title', 'draftsforfriends' ); ?></th>
							<th><?php esc_html_e( 'Link', 'draftsforfriends' ); ?></th>
							<th colspan="2" class="actions"><?php esc_html_e( 'Actions', 'draftsforfriends' ); ?></th>
							<th><?php esc_html_e( 'Expires After', 'draftsforfriends' ) ?></th>
						</tr>
					</thead>
					<tbody>
					<?php
						$shared_posts = $this->get_shared();
						foreach($shared_posts as $share):
							$post = get_post( $share['id'] );
							$url  = get_bloginfo('url') . '/?p=' . $post->ID . '&draftsforfriends='. $share['key'];
							$delete_nonce = wp_create_nonce( 'drafts_for_friends_delete' );
					?>
							<tr>
								<td><?php echo esc_html( $post->ID ); ?></td>
								<td><?php echo esc_html( $post->post_title ); ?></td>
								<!-- TODO: make the draft link selecatble -->
								<td>
									<a href="<?php echo esc_attr( $url ); ?>"><?php echo esc_html( $url ); ?></a>
								</td>
								<td class="actions">
									<a class="draftsforfriends-extend edit" id="draftsforfriends-extend-link-<?php echo esc_attr( $share['key'] ); ?>"
										href="javascript:draftsforfriends.toggle_extend('<?php echo esc_attr( $share['key'] ); ?>');">
											<?php esc_html_e( 'Extend', 'draftsforfriends' ); ?>
									</a>
									<form class="draftsforfriends-extend" id="draftsforfriends-extend-form-<?php echo esc_attr( $share['key'] ); ?>"
										action="" method="post">
										<?php wp_nonce_field( 'drafts_for_friends_extend', 'extend_nonce' ); ?>
										<input type="hidden" name="action" value="extend" />
										<input type="hidden" name="key" value="<?php echo esc_attr( $share['key'] ); ?>" />
										<input type="submit" class="button" name="draftsforfriends_extend_submit"
											value="<?php esc_html_e( 'Extend', 'draftsforfriends' ); ?>"/>
											<?php esc_html_e( 'by', 'draftsforfriends' );?>
											<?php echo $this->tmpl_measure_select(); ?>				
										<a class="draftsforfriends-extend-cancel"
											href="javascript:draftsforfriends.cancel_extend('<?php echo esc_attr( $share['key'] ); ?>');">
											<?php esc_html_e( 'Cancel', 'draftsforfriends' ); ?>
										</a>
									</form>
								</td>
								<td class="actions">
									<a class="delete" href="edit.php?page=<?php echo esc_attr( plugin_basename(__FILE__) ); ?>&amp;action=delete&amp;key=<?php echo esc_attr( $share['key'] ); ?>&amp;nonce=<?php echo $delete_nonce; ?>">
										<?php esc_html_e( 'Delete', 'draftsforfriends' ); ?>
									</a>
								</td>
								<td><?php echo esc_html( $this->get_expiration_status( $share['expires'] ) ); ?></td>
							</tr>
					<?php
						endforeach;
						if ( empty( $shared_posts ) ):
					?>
							<tr>
								<td colspan="5">
									<?php esc_html_e( 'No shared drafts!', 'draftsforfriends' ); ?>
								</td>
							</tr>
						<?php endif; ?>
					</tbody>
				</table>
				<h3><?php esc_html_e( 'Drafts for Friends', 'draftsforfriends' ); ?></h3>
				<form id="draftsforfriends-share" action="" method="post">
					<?php wp_nonce_field( 'drafts_for_friends_add', 'add_nonce' ); ?>
					<p>
						<select id="draftsforfriends-postid" name="post_id">
						<option value=""><?php esc_html_e( 'Choose a draft', 'draftsforfriends' ); ?></option>
					<?php
					foreach( $drafts as $draft ):
						if ( $draft[1] ):
					?>
							<option value="" disabled="disabled"></option>
							<option value="" disabled="disabled"><?php echo esc_html( $draft[0] ); ?></option>
						<?php
							foreach( $draft[2] as $sub_type_draft ):
								if ( empty( $sub_type_draft->post_title ) ) continue;
						?>
								<option value="<?php echo esc_attr( $sub_type_draft->ID ); ?>"><?php echo esc_html( $sub_type_draft->post_title ); ?></option>
					<?php
							endforeach;
						endif;
					endforeach;
					?>
						</select>
					</p>
					<p>
						<input type="submit" class="button" name="draftsforfriends_submit"
							value="<?php esc_html_e( 'Share it', 'draftsforfriends' ); ?>" />
						<?php esc_html_e( 'for', 'draftsforfriends' ); ?>
						<?php echo $this->tmpl_measure_select(); ?>.
					</p>
				</form>
			</div>
		<?php
	}

	/**
	 * Checks if friend can view against list of shared posts
	 * 
	 * @access private
	 * @param int $post_id Post ID
	 * @return bool 
	 */
	private function can_view( $post_id ) {
		foreach( $this->admin_options as $option ) {
			$shares = $option['shared'];
			foreach( $shares as $share ) {
				if ( $share['key'] == $_GET['draftsforfriends'] && $post_id ) {
					return true;
		  		}
		 	}
		}
		return false;
	}

	/**
	 * Callback action to post_results hook that to checks if user can view the post
	 * 
	 * @access public
	 * @param array $posts Array of posts
	 * @return array 
	 */
	public function posts_results_intercept( $posts ) {
		if ( 1 != count( $posts ) ) return  $posts ; 
		
		$post = $posts[0];
		$status = get_post_status( $post );
		if ( 'publish' != $status && $this->can_view( $post->ID ) ) {
			$this->shared_post = $post;
		}
		return $posts;
	}

	/**
	 * Callback action to the_posts to show the shared posts
	 * 
	 * @access public
	 * @param array $posts Array of posts
	 * @return array 
	 */
	public function the_posts_intercept( $posts ){
		if ( empty( $posts ) && ! is_null( $this->shared_post ) ) {
			return array( $this->shared_post );
		} else {
			$this->shared_post = null;
			return $posts;
		}
	}

	/**
	 * Builds component to select share time
	 * 
	 * @access private
	 * @return void
	 */
	private function tmpl_measure_select() {
		$secs  = __( 'seconds', 'draftsforfriends' );
		$mins  = __( 'minutes', 'draftsforfriends' );
		$hours = __( 'hours'  , 'draftsforfriends' );
		$days  = __( 'days'   , 'draftsforfriends' );

		return <<<SELECT
			<input name="expires" type="text" value="2" size="4"/>
			<select name="measure">
				<option value="s">$secs</option>
				<option value="m">$mins</option>
				<option value="h" selected="selected">$hours</option>
				<option value="d">$days</option>
			</select>
SELECT;
	}

	/**
	 * Sets CSS styling for plugin admin page
	 * 
	 * @access public
	 * @return void 
	 */
	public function print_admin_css() {
		?>
			<style type="text/css">
				a.draftsforfriends-extend, a.draftsforfriends-extend-cancel { display: none; }
				form.draftsforfriends-extend { white-space: nowrap; }
				form.draftsforfriends-extend, form.draftsforfriends-extend input, form.draftsforfriends-extend select { font-size: 11px; }
				th.actions, td.actions { text-align: center; }
			</style>
		<?php
	}

	/**
	 * Sets scripts for plugin admin page
	 * 
	 * @access public
	 * @return void 
	 */
	public function print_admin_js() {
		?>
			<script type="text/javascript">
				jQuery(function() {
					jQuery('form.draftsforfriends-extend').hide();
					jQuery('a.draftsforfriends-extend').show();
					jQuery('a.draftsforfriends-extend-cancel').show();
					jQuery('a.draftsforfriends-extend-cancel').css('display', 'inline');
				});
				window.draftsforfriends = {
					toggle_extend: function(key) {
						jQuery('#draftsforfriends-extend-form-'+key).show();
						jQuery('#draftsforfriends-extend-link-'+key).hide();
						jQuery('#draftsforfriends-extend-form-'+key+' input[name="expires"]').focus();
					},
					cancel_extend: function(key) {
						jQuery('#draftsforfriends-extend-form-'+key).hide();
						jQuery('#draftsforfriends-extend-link-'+key).show();
					}
				};
			</script>
		<?php
	}
}

new draftsforfriends(); 